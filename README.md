# rstatus

rstatus is a simple widget-based status monitor written in Rust, to be used in
any window manager that uses stdin to fill its status bar.  
It uses a separate thread for every widget, with each their own update
interval.

rstatus is incomplete, not ready for use, and started mainly as a personal
project to *learn* Rust. The code is most likely **very** far from idiomatic.

## Features

* Load average
* Kernel (`uname -r`)
* Memory usage
* Date and time

## TODO

* Add more widgets
* Allow output format to be customized based on configuration
* Allow widgets to be activated based on configuration
* more
