/*
 * Copyright (C) 2018 Kenneth De Winter <kenneth@dewinter.online>
 *
 * This file is part of rstatus.
 *
 * rstatus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rstatus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rstatus. If not, see <http://www.gnu.org/licenses/>.
 */

use util;
use std::sync::mpsc::Sender;
use std::process::Command;

pub fn register_widget(tx: &Sender<(&'static str, Option<String>)>) {
    let kernel = match Command::new("uname").arg("-r").output() {
        Ok(output) => {
            match String::from_utf8(output.stdout) {
                Ok(stdout) => Some(util::chomp_newline(stdout)),
                Err(_)     => None
            }
        },
        Err(_) => None
    };

    tx.send(("kernel", kernel)).unwrap()
}
