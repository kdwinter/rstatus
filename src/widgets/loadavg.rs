/*
 * Copyright (C) 2018 Kenneth De Winter <kenneth@dewinter.online>
 *
 * This file is part of rstatus.
 *
 * rstatus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rstatus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rstatus. If not, see <http://www.gnu.org/licenses/>.
 */

extern crate libc;

use widget::Widget;
use std::sync::mpsc::Sender;
// NOTE: Why is self:: necessary? Because it's in a module or?
use self::libc::c_int;

#[link(name = "c")]
extern "C" {
    fn getloadavg(loadavg: *mut f64, nelem: c_int) -> c_int;
}

pub fn register_widget(tx: &Sender<(&'static str, Option<String>)>, interval: u64) {
    let widget = Widget { name: "loadavg", interval: interval };

    widget.start_thread(&tx, || {
        /*match File::open("/proc/loadavg") {
            Ok(mut file) => {
                let mut contents = String::new();

                match file.read_to_string(&mut contents) {
                    Ok(_) => {
                        let split: Vec<String> = contents.split(" ").map(|s| s.to_string()).collect();
                        Some(util::chomp_newline(split[..3].join(" ")))
                    },
                    Err(_) => None
                }
            },
            Err(_) => None
        }*/

        let mut loads: [f64; 3] = [0.0, 0.0, 0.0];

        if unsafe { getloadavg(&mut loads[0], 3) } != 3 {
            return None
        }

        Some(format!("{} {} {}", loads[0], loads[1], loads[2]))
    });
}
