/*
 * Copyright (C) 2018 Kenneth De Winter <kenneth@dewinter.online>
 *
 * This file is part of rstatus.
 *
 * rstatus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rstatus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rstatus. If not, see <http://www.gnu.org/licenses/>.
 */

extern crate regex;

use widget::Widget;
use std::sync::mpsc::Sender;
use std::fs::File;
use std::io::Read;

fn parse_meminfo(contents: &str, value: &str) -> Option<u64> {
    let regex: regex::Regex = regex::Regex::new(
        &format!("{}:\\s+([0-9]+)", value)
    ).expect("Couldn't create regex object");

    match regex.captures(&contents).unwrap()[1].parse::<u64>() {
        Ok(capture) => Some(capture),
        Err(_)      => None
    }
}

pub fn register_widget(tx: &Sender<(&'static str, Option<String>)>, interval: u64) {
    let widget = Widget { name: "memory", interval: interval };

    widget.start_thread(&tx, || {
        let mut file = File::open("/proc/meminfo").expect("Couldn't open /proc/meminfo");
        let mut contents = String::new();

        file.read_to_string(&mut contents).expect("Couldn't parse /proc/meminfo");

        let mem_total   = parse_meminfo(&contents, "MemTotal")?;
        let mem_free    = parse_meminfo(&contents, "MemFree")?;
        let mem_buffers = parse_meminfo(&contents, "Buffers")?;
        let mem_cached  = parse_meminfo(&contents, "Cached")?;

        let mem_used = (mem_total - mem_free - mem_cached - mem_buffers) as f64;
        let percentage: u32 = ((mem_used / mem_total as f64) * 100f64) as u32;

        let t: f64 = 1024f64;
        if mem_used > 1024000f64 {
            Some(format!("{:.2} GiB ({}%)", mem_used / t / t, percentage))
        } else {
            Some(format!("{} MiB ({}%)", (mem_used / t).round(), percentage))
        }
    });
}
