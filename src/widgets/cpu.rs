/*
 * Copyright (C) 2018 Kenneth De Winter <kenneth@dewinter.online>
 *
 * This file is part of rstatus.
 *
 * rstatus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rstatus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rstatus. If not, see <http://www.gnu.org/licenses/>.
 */

use widget::Widget;
use std::sync::{Arc, Mutex};
use std::sync::mpsc::Sender;
use std::fs::File;
use std::io::BufReader;
use std::io::prelude::*;

fn parse_cpuline() -> Vec<u64> {
    let file = File::open("/proc/stat").expect("Couldn't open /proc/stat");
    let mut buffer = BufReader::new(file);
    let mut first_line = String::new();

    buffer.read_line(&mut first_line).expect("Couldn't parse /proc/stat");

    let values = first_line.
        trim().
        split(" ").
        skip_while(|&v| v == "cpu" || v == "").
        map(|v| v.parse::<u64>().expect("Couldn't parse cpuline")).
        collect();

    values
}

pub fn register_widget(tx: &Sender<(&'static str, Option<String>)>, interval: u64) {
    let widget = Widget { name: "cpu", interval: interval };

    let a = Arc::new(Mutex::new(parse_cpuline()));
    let ta = a.clone();

    widget.start_thread(&tx, move || {
        let mut mta = ta.lock().unwrap();
        let b = parse_cpuline();
        let mut usage = 0;
        let divisor = (b[0]+b[1]+b[2]+b[3]+b[4]+b[5]+b[6]) -
            (mta[0]+mta[1]+mta[2]+mta[3]+mta[4]+mta[5]+mta[6]);

        // Calculate
        // FIXME: Needs revision
        if divisor != 0 {
            usage = 100 * (
                (b[0]+b[1]+b[2]+b[5]+b[6]) -
                (mta[0]+mta[1]+mta[2]+mta[5]+mta[6])
            ) / divisor;
        }

        // Store values
        for i in 0..8 {
            mta[i] = b[i];
        }

        Some(format!("{:.1}%", usage))
    });
}
