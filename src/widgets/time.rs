/*
 * Copyright (C) 2018 Kenneth De Winter <kenneth@dewinter.online>
 *
 * This file is part of rstatus.
 *
 * rstatus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rstatus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rstatus. If not, see <http://www.gnu.org/licenses/>.
 */

use widget::Widget;
use std::sync::mpsc::Sender;

pub fn register_widget(tx: &Sender<(&'static str, Option<String>)>, interval: u64) {
    let widget = Widget { name: "time", interval: interval };

    widget.start_thread(&tx, || {
        match ::time::strftime("%d/%m/%Y %H:%M", &::time::now()) {
            Ok(val) => Some(val),
            Err(_)  => None
        }
    });
}
