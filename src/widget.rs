/*
 * Copyright (C) 2018 Kenneth De Winter <kenneth@dewinter.online>
 *
 * This file is part of rstatus.
 *
 * rstatus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rstatus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rstatus. If not, see <http://www.gnu.org/licenses/>.
 */

use std::{thread, time as std_time};
use std::sync::Arc;
use std::sync::mpsc::Sender;
use std::sync::atomic::Ordering;
use RUNNING;

#[derive(Clone)]
pub struct Widget {
    pub name: &'static str,
    pub interval: u64
}

impl Widget {
    pub fn start_thread<F> (
        &self,
        tx:       &Sender<(&'static str, Option<String>)>,
        callback: F
    ) where F: Fn() -> Option<String> + Send + Sync + 'static {
        let thread_self = Arc::new(self.clone());
        let thread_tx = tx.clone();
        let thread_running = RUNNING.clone();

        thread::spawn(move || {
            debug!("> Starting {} thread", thread_self.name);

            loop {
                if !thread_running.load(Ordering::SeqCst) {
                    break;
                }

                let new_value = callback();
                thread_tx.send((thread_self.name, new_value)).unwrap();

                let sleep_duration = std_time::Duration::from_millis(thread_self.interval * 1000);
                thread::sleep(sleep_duration);
            }
        });
    }
}
