/*
 * Copyright (C) 2018 Kenneth De Winter <kenneth@dewinter.online>
 *
 * This file is part of rstatus.
 *
 * rstatus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rstatus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rstatus. If not, see <http://www.gnu.org/licenses/>.
 */

pub fn chomp_newline(text: String) -> String {
    String::from(text.trim())
}

// TODO: Parse widget format from some kind of configuration file!
pub fn format_widget(name: &str, value: &str) -> String {
    format!("{{\"full_text\": \"{}: {}\"}},", name, value)
}
