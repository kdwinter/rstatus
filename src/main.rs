/*
 * Copyright (C) 2018 Kenneth De Winter <kenneth@dewinter.online>
 *
 * This file is part of rstatus.
 *
 * rstatus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rstatus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rstatus. If not, see <http://www.gnu.org/licenses/>.
 */

extern crate time;
extern crate ctrlc;
extern crate getopts;
#[macro_use] extern crate lazy_static;
#[macro_use] extern crate log;

use std::{thread, process, time as std_time};
use std::sync::atomic::{AtomicBool, Ordering};
use std::sync::{Arc, mpsc};
use std::sync::mpsc::{Sender, Receiver};
use std::collections::BTreeMap;
use std::env;
use getopts::Options;

// rstatus version
const VERSION: Option<&str> = option_env!("CARGO_PKG_VERSION");

lazy_static! {
    // Global running state
    static ref RUNNING: Arc<AtomicBool> = Arc::new(AtomicBool::new(true));
    // Valid output formats
    static ref OUTPUT_FORMATS: Vec<String> = vec!["i3".to_owned()];
}

mod util;
mod widget;
mod widgets;

fn print_usage(program: &str, options: Options) {
    let brief = format!("Usage: {} [options]", program);
    println!("{}", options.usage(&brief));
    process::exit(0);
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let program = args[0].clone();

    let mut options = Options::new();
    options.optopt("f", "format", "set output format [i3]", "FORMAT");
    options.optflag("h", "help", "print this help menu");

    let matches = match options.parse(&args[1..]) {
        Ok(m)  => m,
        Err(f) => panic!(f.to_string())
    };

    if matches.opt_present("h") {
        print_usage(&program, options);
    }

    let output_format = match matches.opt_str("f") {
        Some(f) => f,
        None    => "i3".to_owned()
    };

    if !OUTPUT_FORMATS.contains(&output_format) {
        eprintln!("Invalid output format ({}).", output_format);
        process::exit(1);
    }

    debug!("> Running rstatus v{} for {}", VERSION.unwrap_or("unknown"), output_format);

    // Inter-thread communication channels
    let (tx, rx): (
        Sender<(&'static str, Option<String>)>,
        Receiver<(&'static str, Option<String>)>
    ) = mpsc::channel();

    // Cleanly stop threads when rstatus is closed/interrupted.
    let r = RUNNING.clone();
    match ctrlc::set_handler(move || r.store(false, Ordering::SeqCst)) {
        Ok(_)  => ..,
        Err(_) => panic!("Couldn't set CTRL+C handler")
    };

    ///// Register widgets
    // TODO: Load these based on configuration
    widgets::time::register_widget(&tx, 1);
    widgets::kernel::register_widget(&tx);
    widgets::loadavg::register_widget(&tx, 11);
    widgets::memory::register_widget(&tx, 7);
    widgets::cpu::register_widget(&tx, 3);

    // A hash containing up-to-date results, populated by the running threads
    let mut results = BTreeMap::new();

    // Print i3bar prefix json
    println!("{{ \"version\": 1 }}");
    println!("[");
    println!("[],");

    // Main thread
    loop {
        if !RUNNING.load(Ordering::SeqCst) {
            break;
        }

        // Main thread sleeps 1 second by default
        thread::sleep(std_time::Duration::from_millis(1000));

        // Receive widget thread updates if any
        match rx.recv() {
            Ok(result) => {
                if let Some(val) = result.1 {
                    results.insert(result.0, val);
                }
            },
            Err(_) => ()
        };

        // Build the status output
        let mut status = String::new();
        for (name, ref value) in results.iter() {
            status.push_str(&util::format_widget(name, &value));
        }

        // Print the up-to-date status
        // TODO: Output this in a format most window managers understand? Or
        //       based on a commandline argument.
        println!("[{}],", status);
    }

    process::exit(0);
}
